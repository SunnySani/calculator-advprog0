package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpicyTest {
    private Class<?> spicyTest;

    @BeforeEach
    public void setUp() throws Exception {
        spicyTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
    }

    @Test
    public void testspicyTestIsPublicClass() {
        assertTrue(Modifier.isPublic(spicyTest.getModifiers()));
    }

    @Test
    public void testspicyTestIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(spicyTest.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testspicyTestOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = spicyTest.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSpicyDescriptionMethod() throws Exception {
        Spicy spicy = new Spicy();
        assertEquals("Adding Liyuan Chili Powder...",spicy.getDescription());
    }
}
