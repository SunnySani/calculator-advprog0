package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngridientFactoryTest {
    private Class<?> ingridientFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingridientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory");
    }

    @Test
    public void testIngridientFactoryIsAPublicInterface() {
        int classModifiers = ingridientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngridientFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method createFlavor = ingridientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
    }

    @Test
    public void testIngridientFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method createMeat = ingridientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
    }

    @Test
    public void testIngridientFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method createNoodle = ingridientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
    }

    @Test
    public void testIngridientFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method createTopping = ingridientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
    }
}