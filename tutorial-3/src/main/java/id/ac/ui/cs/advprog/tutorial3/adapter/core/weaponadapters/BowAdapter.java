package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    boolean isAimShot = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return this.getHolderName() +  " attacked with " + this.getName() +
                " (normal attack): " + bow.shootArrow(isAimShot) + "!";
    }

    @Override
    public String chargedAttack() {
        if (!isAimShot) {
            isAimShot = true;
            return this.getHolderName() +  " attacked with " + this.getName() +
                    " (charged attack): Entered aim shot mode";
        } else {
            isAimShot = false;
            return this.getHolderName() +  "attacked with " + this.getName() +
                    " (charged attack): Entered normal shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
