package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell: spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        Collections.reverse(this.spells);
        for (Spell spell: spells) {
            spell.undo();
        }
        // Returns ArrayList spells to normal
        Collections.reverse(this.spells);
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
