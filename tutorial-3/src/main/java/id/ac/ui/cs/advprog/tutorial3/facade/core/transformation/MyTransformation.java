package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini melakukan Caesar cipher dengan menggeser kelipatan 2
 */

/**
 * Langkah Transformasi Ceaser Cipher berkelipatan 2
 *
 * 1. Ada variable key yang menjelaskan berapa kali kelipatan dua yang ingin digeser
 * 2. Method encode menyatakan bahwa karakter pada string akan digeser ke kanan
 * 3. Sedangkan metode decode menyatakan bahwa karakter pada string akan di geser ke kiri
 * 4. Ditambahkan method process untuk mempersingkat code dan input boolean encode
 * 5. Berikut adalah method yang dilakukan di process
 *  5.1 Ambil text dari Spell
 *  5.2 Tentukan apakah boolean encode true atau false untuk menentukan nilai selector +2 atau -2
 *   5.2.1 Jika encode true maka nilainya bersifat positif yang menandakan karakter akan digeser ke index senlanjutnya
 *         (Kanan)
 *   5.2.2 Jika encode false maka karakter akan digeser ke index sebelum-sebelumnya (Kiri)
 *  5.3 Buat char[] (List of char) untuk hasil akhir dengan size sepanjang text yang didapat dari 5.1
 *  5.4 Cek untuk setiap index lalu buat index yang baru dengan pertambahan key * selector
 *   5.4.1 setelah dapat index baru, jangan lupa untuk di-mod-kan agar tidak indexOutOfBound (index + key * selector) %
 *         length
 *  5.5 Taruh karakter yang berada di index itu ke index baru dalam char[] yang telah dibuat
 *  5.6 return Spell dengan text gabungan dari char[] dan codex yang sama
 */

public class MyTransformation {
    private int key;

    public MyTransformation(int key){
        this.key = key;
    }

    public MyTransformation(){ this(1); }

    public Spell encode(Spell spell) { return process(spell, true); }

    public Spell decode(Spell spell) { return process(spell, false); }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 2 : -2;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int newIdx = (i + key * selector) % n;
            newIdx = newIdx < 0 ? n + newIdx : newIdx;
            res[newIdx] = text.charAt(i);
        }

        return new Spell(new String(res), codex);
    }
}
