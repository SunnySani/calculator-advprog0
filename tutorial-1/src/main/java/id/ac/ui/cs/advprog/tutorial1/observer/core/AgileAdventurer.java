package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        if (this.guild.getQuestType().equals("D"))
            this.getQuests().add(this.guild.getQuest());
        else if (this.guild.getQuestType().equals("R"))
            this.getQuests().add(this.guild.getQuest());
    }
}
