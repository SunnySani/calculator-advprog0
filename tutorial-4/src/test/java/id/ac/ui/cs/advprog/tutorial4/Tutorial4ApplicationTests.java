package id.ac.ui.cs.advprog.tutorial4;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial4ApplicationTests {
    @Test
    void contextLoads() {
    }

    @Test
    void testSpringInstanceCanBeMade(){
        Tutorial4Application.main(new String[] {});
    }
}