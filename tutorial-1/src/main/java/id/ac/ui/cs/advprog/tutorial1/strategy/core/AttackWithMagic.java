package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack(){return "Attack using Magic";}
    public String getType(){return "Magic";}
}
