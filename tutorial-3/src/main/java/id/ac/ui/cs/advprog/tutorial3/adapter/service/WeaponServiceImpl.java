package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    // TODO: implement me

    boolean paramWeaponRepoIsCompleted;
    @Override
    public List<Weapon> findAll() {
        Weapon temp;
        if (!paramWeaponRepoIsCompleted) {
            for (Bow bow: bowRepository.findAll()) {
                temp = new BowAdapter(bow);
                weaponRepository.save(temp);
            }
            for (Spellbook spellbook: spellbookRepository.findAll()) {
                temp = new SpellbookAdapter(spellbook);
                weaponRepository.save(temp);
            }
            paramWeaponRepoIsCompleted = true;
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        if (attackType == 0)
            logRepository.addLog(weaponRepository.findByAlias(weaponName).normalAttack());
        else
            logRepository.addLog(weaponRepository.findByAlias(weaponName).chargedAttack());
        weaponRepository.save(weaponRepository.findByAlias(weaponName)); // ngikutin test, tapi ga berguna.
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
