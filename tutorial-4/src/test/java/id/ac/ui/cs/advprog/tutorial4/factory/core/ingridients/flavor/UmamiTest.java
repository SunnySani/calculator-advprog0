package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UmamiTest {
    private Class<?> umamiTest;

    @BeforeEach
    public void setUp() throws Exception {
        umamiTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    public void testumamiTestIsPublicClass() {
        assertTrue(Modifier.isPublic(umamiTest.getModifiers()));
    }

    @Test
    public void testumamiTestIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiTest.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testumamiTestOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = umamiTest.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testUmamiGetDescriptionMethod() throws Exception {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...",umami.getDescription());
    }
}
