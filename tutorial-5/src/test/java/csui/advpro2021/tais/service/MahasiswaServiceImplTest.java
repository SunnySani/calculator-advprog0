package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    private Mahasiswa mahasiswa;

    private MataKuliah matkul;

    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
        matkul = new MataKuliah("AE7625SG", "Bahasa Indoensia", "A");
        log = new Log(1, "01 08:00:00", "01 08:30:00", "test ngajar");
    }

    @Test
    public void testServiceCreateMahasiswa(){
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteMahasiswa(){
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        // if mahasiswa is approved dont delete
        mahasiswa.setApproved(true);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        assertEquals(mahasiswa, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));

        // delete mahasisa
        mahasiswa.setApproved(false);
        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(null);
        assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));

        // if mahasiswa null
        mahasiswaService.deleteMahasiswaByNPM("123");
    }

    @Test
    public void testServiceUpdateMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        String currentIpkValue = mahasiswa.getIpk();
        //Change IPK from 4 to 3
        mahasiswa.setIpk("3");

        lenient().when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);

        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
        assertEquals(resultMahasiswa.getNama(), mahasiswa.getNama());

        Mahasiswa testMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
        assertEquals(testMahasiswa, mahasiswa);
    }

    @Test
    public void testServiceRegisterCourse(){
        // if mahasiswa == null then return null
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(null);
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(null);
        mahasiswaService.registerCourse(mahasiswa.getNpm(), matkul.getKodeMatkul());
        assertEquals(mahasiswa.getMataKuliah(), null);
        assertEquals(mahasiswa.isApproved(), false);

        // if matkul == nul then return null
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(null);
        mahasiswaService.registerCourse(mahasiswa.getNpm(), matkul.getKodeMatkul());
        assertEquals(mahasiswa.getMataKuliah(), null);
        assertEquals(mahasiswa.isApproved(), false);


        // if mahasiswa != null and matkull != null then save
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        mahasiswaService.registerCourse(mahasiswa.getNpm(), matkul.getKodeMatkul());
        assertEquals(mahasiswa.getMataKuliah(), matkul);
        assertEquals(mahasiswa.isApproved(), true);
    }

    @Test
    public void testServiceDropCourse(){
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mahasiswaService.registerCourse(mahasiswa.getNpm(), matkul.getKodeMatkul());
        assertEquals(mahasiswa.getMataKuliah(), matkul);

        // if mahasiswa null then return null
        lenient().when(mahasiswaService.getMahasiswaByNPM("12345")).thenReturn(null);
        assertEquals(null, mahasiswaService.dropCourse("12345"));

        // else
        mahasiswaService.dropCourse(mahasiswa.getNpm());
        assertEquals(mahasiswa.getMataKuliah(), null);
    }

    @Test
    public void testServiceAddLog(){
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);

        mahasiswaService.registerCourse(mahasiswa.getNpm(), matkul.getKodeMatkul());

        lenient().when(mahasiswaService.addLog(mahasiswa.getNpm(), log)).thenReturn(log);

        Log logTest = mahasiswaService.addLog(mahasiswa.getNpm(), log);

        assertEquals(logTest.getMahasiswa(), mahasiswa);

        // if isApproved false, dont add
        mahasiswa.setApproved(false);
        assertEquals(mahasiswaService.addLog(mahasiswa.getNpm(), log), null);
    }
}
