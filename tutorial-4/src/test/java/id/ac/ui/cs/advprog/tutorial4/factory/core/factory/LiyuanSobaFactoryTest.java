package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;

    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryClassIsConcreteClass() {
        int classModifiers = liyuanSobaFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testLiyuanSobaFactoryIsAnIngridientFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory")));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaFactoryCreateFlavorShouldReturnSweet() throws Exception {
        Flavor flavor = liyuanSobaFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Sweet.class);
    }

    @Test
    public void testLiyuanSobaFactoryCreateMeatShouldReturnBeef() throws Exception {
        Meat meat = liyuanSobaFactory.createMeat();
        assertThat(meat).isInstanceOf(Beef.class);
    }

    @Test
    public void testLiyuanSobaFactoryCreateNoodleShouldReturnSoba() throws Exception {
        Noodle noodle = liyuanSobaFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Soba.class);
    }

    @Test public void testLiyuanSobaFactoryCreateToppingShouldReturnMushroom() throws Exception {
        Topping topping = liyuanSobaFactory.createTopping();
        assertThat(topping).isInstanceOf(Mushroom.class);
    }
}
