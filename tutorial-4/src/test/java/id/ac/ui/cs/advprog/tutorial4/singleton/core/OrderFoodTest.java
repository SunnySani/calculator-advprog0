package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

public class OrderFoodTest {

    /*@Mock
    OrderFood orderFood;

    @BeforeEach
    public void setup() throws Exception {
        orderFood = mock(OrderFood.class);
    }*/

    @Test
    public void testGetInstanceIsTheSameForEveryObject() {
        OrderFood orderDrink1 = OrderFood.getInstance();
        OrderFood orderDrink2 = OrderFood.getInstance();

        assertThat(orderDrink1).isEqualToComparingFieldByField(orderDrink2);
    }

    /*@Test
    public void testOrderServiceOrderAFoodImplementedCorrectly() throws Exception {
        doNothing().when(orderFood).setFood(isA(String.class));
    }*/

}
