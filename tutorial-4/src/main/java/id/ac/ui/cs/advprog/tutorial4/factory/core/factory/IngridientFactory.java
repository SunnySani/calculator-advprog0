package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;


import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;


public interface IngridientFactory {

    public Noodle createNoodle();

    public Meat createMeat();

    public Topping createTopping();

    public Flavor createFlavor();
}
