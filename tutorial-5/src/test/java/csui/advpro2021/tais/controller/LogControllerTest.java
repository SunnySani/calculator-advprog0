package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");

        mataKuliah = new MataKuliah("AE7625SG", "Bahasa Indoensia", "A");

        log = new Log(1, "01 08:00:00", "01 08:30:00", "test ngajar");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetLog() throws Exception{
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testControllerGetLogMahasiswa() throws Exception{

        mvc.perform(get("/log/1906192052")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerDeleteMataKuliah() throws Exception {
        mahasiswaService.addLog("1906192052", log);
        mvc.perform(delete("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testgetListLogByMonth() throws Exception {
        mahasiswaService.addLog("1906192052", log);
        mvc.perform(get("/log/getLogReportMahasiswa/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
