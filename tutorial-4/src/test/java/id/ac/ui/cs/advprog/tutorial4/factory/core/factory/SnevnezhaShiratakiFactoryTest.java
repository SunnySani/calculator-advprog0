package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;

    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryClassIsConcreteClass() {
        int classModifiers = snevnezhaShiratakiFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAnIngridientFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateFlavorShouldReturnUmami() throws Exception {
        Flavor flavor = snevnezhaShiratakiFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Umami.class);
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateMeatShouldReturnFish() throws Exception {
        Meat meat = snevnezhaShiratakiFactory.createMeat();
        assertThat(meat).isInstanceOf(Fish.class);
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateNoodleShouldReturnShirataki() throws Exception {
        Noodle noodle = snevnezhaShiratakiFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Shirataki.class);
    }

    @Test public void testSnevnezhaShiratakiFactoryCreateToppingShouldReturnFlower() throws Exception {
        Topping topping = snevnezhaShiratakiFactory.createTopping();
        assertThat(topping).isInstanceOf(Flower.class);
    }
}
