#Fitur tutorial 5

##Relation one to many
Mahasiswa --(Menjadi Asdos)--> MataKuliah
Log --(dimiliki)--> Mahasiswa

##Dalam entity Log, disimpan start, end, description
Tentu saja juga ada Mahasiswa sebagai relation

##Mahasiswa bisa daftar sebagai Asdos dan langsung diterima

##Asumsi: Mahasiswa bisa menulis log
Hanya bila ia menjadi asdos. Maka saya menambahkan variabel boolean approved

##Mengembalikan Laporan Pembayaran untuk mahasiswa
Laporan Pembayaran berisi: Bulan, JamKerja, JumlahBayaran

##Entitiy MataKuliah tidak bisa dihapus apabila memiliki Asdos

##Entity Mahasiswa tidak bisa dihapus apabila merupakan Asdos
Hal ini ditunjukkan dari variabel boolean approved

