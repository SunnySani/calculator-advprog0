package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MyTransformationTest {
    private Class<?> myClass;

    @BeforeEach
    public void setup() throws Exception {
        myClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation");
    }

    @Test
    public void testAbyssalHasEncodeMethod() throws Exception {
        Method translate = myClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransformationEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "rdSafira and I went to a blacksmith to forge our swo";

        Spell result = new MyTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransformationCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fira and I went to a blacksmith to forge our swordSa";

        Spell result = new MyTransformation(-1).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransformationHasDecodeMethod() throws Exception {
        Method translate = myClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransformationDecodesCorrectly() throws Exception {
        String text = "rdSafira and I went to a blacksmith to forge our swo";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new MyTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testAbyssalDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "fira and I went to a blacksmith to forge our swordSa";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new MyTransformation(-1).decode(spell);
        assertEquals(expected, result.getText());
    }
}
