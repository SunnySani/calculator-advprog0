package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;

import java.util.Map;

public interface LogService {
    Iterable<Log> getListLog();

    Iterable<Log> getListLogByMahasiswa(String npm);

    void deleteLogByIdLog(int idLog);

    Iterable<Map<String, Object>> getLogReportMahasiswa(String npm);
}
