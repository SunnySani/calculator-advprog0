package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class OrderDrinkTest {

    /*@Mock
    OrderDrink orderDrink;

    @BeforeEach
    public void setup() throws Exception {
        orderDrink = mock(OrderDrink.class);
    }*/

    @Test
    public void testGetInstanceIsTheSameForEveryObject() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();

        assertThat(orderDrink1).isEqualToComparingFieldByField(orderDrink2);
    }

    /*@Test
    public void testOrderServiceOrderADrinkImplementedCorrectly() throws Exception {
        doNothing().when(orderDrink).setDrink(isA(String.class));
    }*/

}
