package csui.advpro2021.tais.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "log")
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitialize", "handler"})
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idLog", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "startTime", nullable = false)
    @JsonFormat(pattern="MM HH:mm:ss")
    private String startTime;

    @Column(name = "endTime", nullable = false)
    @JsonFormat(pattern="MM HH:mm:ss")
    private String endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;

    public Log(int idLog, String startTime, String endTime, String deskripsi) {
        this.idLog = idLog;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}
