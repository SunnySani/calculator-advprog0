package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Class<?> menuClass;

    @Mock
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasgetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasgetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasgetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
        assertEquals(0, getFlavor.getParameterCount());
    }

    @Test
    public void testOutputForInuzumaRamen() throws Exception {
        Menu inuzumaRamen = new InuzumaRamen("test");
        assertEquals("test", inuzumaRamen.getName());
        assertEquals("Adding Guahuan Boiled Egg Topping", inuzumaRamen.getTopping().getDescription());
        assertEquals("Adding Inuzuma Ramen Noodles...", inuzumaRamen.getNoodle().getDescription());
        assertEquals("Adding Tian Xu Pork Meat...", inuzumaRamen.getMeat().getDescription());
        assertEquals("Adding Liyuan Chili Powder...", inuzumaRamen.getFlavor().getDescription());
    }

    @Test
    public void testOutputForMondoUdon() throws Exception {
        Menu mondoUdon = new MondoUdon("test");
        assertEquals("test", mondoUdon.getName());
        assertEquals("Adding Shredded Cheese Topping...", mondoUdon.getTopping().getDescription());
        assertEquals("Adding Mondo Udon Noodles...", mondoUdon.getNoodle().getDescription());
        assertEquals("Adding Wintervale Chicken Meat...", mondoUdon.getMeat().getDescription());
        assertEquals("Adding a pinch of salt...", mondoUdon.getFlavor().getDescription());
    }

    @Test
    public void testOutputForSnevnezhaShirataki() throws Exception {
        Menu snevnezhaShirataki = new SnevnezhaShirataki("test");
        assertEquals("test", snevnezhaShirataki.getName());
        assertEquals("Adding Xinqin Flower Topping...", snevnezhaShirataki.getTopping().getDescription());
        assertEquals("Adding Snevnezha Shirataki Noodles...", snevnezhaShirataki.getNoodle().getDescription());
        assertEquals("Adding Zhangyun Salmon Fish Meat...", snevnezhaShirataki.getMeat().getDescription());
        assertEquals("Adding WanPlus Specialty MSG flavoring...", snevnezhaShirataki.getFlavor().getDescription());
    }

    @Test
    public void testOutputForLiyuanSoba() throws Exception {
        Menu liyuanSoba = new LiyuanSoba("test");
        assertEquals("test", liyuanSoba.getName());
        assertEquals("Adding Shiitake Mushroom Topping...", liyuanSoba.getTopping().getDescription());
        assertEquals("Adding Liyuan Soba Noodles...", liyuanSoba.getNoodle().getDescription());
        assertEquals("Adding Maro Beef Meat...", liyuanSoba.getMeat().getDescription());
        assertEquals("Adding a dash of Sweet Soy Sauce...", liyuanSoba.getFlavor().getDescription());
    }
}
