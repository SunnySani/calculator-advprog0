package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        // TODO: complete me
        return this.getHolderName() +  " attacked with " + this.getName() +
                " (normal attack): pluk pluk";
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me
        return this.getHolderName() +  " attacked with " + this.getName() +
                " (charged attack): SPIKED";
    }

    @Override
    public String getName() {
        return "Full Moon Pike";
    }

    @Override
    public String getHolderName() { return holderName; }
}
