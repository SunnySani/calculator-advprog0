package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class MondoUdonTest {
    private Class<?> mondoClass;

    @BeforeEach
    public void setUp() throws Exception {
        mondoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoClass.getModifiers()));
    }
}
