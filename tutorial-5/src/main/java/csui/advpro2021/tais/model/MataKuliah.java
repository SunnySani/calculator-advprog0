package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mata_kuliah")
@Data
@NoArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitialize", "handler"})
public class MataKuliah {
    @Id
    @Column(name = "kode_matkul", updatable = false)
    private String kodeMatkul;

    @Column(name = "nama_matkul")
    private String nama;

    @Column(name = "prodi")
    private String prodi;

    @OneToMany(mappedBy = "mataKuliah")
    @JsonManagedReference
    private List<Mahasiswa> mahasiswa;

    public MataKuliah(String kodeMatkul, String nama, String prodi) {
        this.kodeMatkul = kodeMatkul;
        this.nama = nama;
        this.prodi = prodi;
        mahasiswa = new ArrayList<>();
    }
}
