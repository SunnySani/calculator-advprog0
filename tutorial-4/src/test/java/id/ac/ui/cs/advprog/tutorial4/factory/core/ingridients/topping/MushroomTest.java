package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MushroomTest {
    private Class<?> mushroomTest;

    @BeforeEach
    public void setUp() throws Exception {
        mushroomTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
    }

    @Test
    public void testmushroomTestIsPublicClass() {
        assertTrue(Modifier.isPublic(mushroomTest.getModifiers()));
    }

    @Test
    public void testmushroomTestIsATopping() {
        Collection<Type> interfaces = Arrays.asList(mushroomTest.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testmushroomTestOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = mushroomTest.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMushroomGetDescriptionMethod() throws Exception {
        Mushroom mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...",mushroom.getDescription());
    }

}
