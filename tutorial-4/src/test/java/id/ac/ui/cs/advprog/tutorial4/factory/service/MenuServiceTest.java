package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private Class<?> menuServiceClass;
    private MenuService menuServiceImpl;

    @Mock
    MenuRepository repo;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
        menuServiceImpl = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceIsAPublicInterface() {
        int classModifiers = menuServiceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMenus.getParameterCount());
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Class<?>[] createMenuArgs = new Class[2];
        createMenuArgs[0] = String.class;
        createMenuArgs[1] = String.class;
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", createMenuArgs);
        int methodModifiers = createMenu.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(2, createMenu.getParameterCount());
    }

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        assertEquals(4, menuServiceImpl.getMenus().size());
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {
        menuServiceImpl.createMenu("TestMenu1", "Soba");
        menuServiceImpl.createMenu("TestMenu2", "Udon");

        assertEquals(6, menuServiceImpl.getMenus().size());
    }
}