package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;

    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryClassIsConcreteClass() {
        int classModifiers = mondoUdonFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMondoUdonFactoryIsAnIngridientFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory")));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonFactoryCreateToppingShouldReturnCheese() throws Exception {
        Topping topping = mondoUdonFactory.createTopping();
        assertThat(topping).isInstanceOf(Cheese.class);
    }

    @Test
    public void testMondoUdonFactoryCreateFlavorShouldReturnSalty() throws Exception {
        Flavor flavor = mondoUdonFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Salty.class);
    }

    @Test
    public void testMondoUdonFactoryCreateMeatShouldReturnChicken() throws Exception {
        Meat meat = mondoUdonFactory.createMeat();
        assertThat(meat).isInstanceOf(Chicken.class);
    }

    @Test
    public void testMondoUdonFactoryCreateNoodleShouldReturnUdon() throws Exception {
        Noodle noodle = mondoUdonFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Udon.class);
    }
}
