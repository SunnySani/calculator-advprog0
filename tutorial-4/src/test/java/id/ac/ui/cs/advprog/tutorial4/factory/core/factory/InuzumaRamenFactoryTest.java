package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;

    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryClassIsConcreteClass() {
        int classModifiers = inuzumaRamenFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testInuzumaRamenFactoryIsAnIngridientFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory")));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }
    @Test
    public void testInuzumaRamenFactoryCreateMeatPork() throws Exception {
        Meat meat = inuzumaRamenFactory.createMeat();
        assertThat(meat).isInstanceOf(Pork.class);
    }

    @Test
    public void testInuzumaRamenFactoryCreateFlavorSpicy() throws Exception {
        Flavor flavor = inuzumaRamenFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Spicy.class);
    }

    @Test
    public void testInuzumaRamenFactoryCreateToppingBoiledEgg() throws Exception {
        Topping topping = inuzumaRamenFactory.createTopping();
        assertThat(topping).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testInuzumaRamenFactoryCreateNoodleRamen() throws Exception {
        Noodle noodle = inuzumaRamenFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Ramen.class);
    }
}
