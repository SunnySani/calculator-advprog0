package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class InuzumaRamenTest {
    private Class<?> inuzumaClass;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaClass.getModifiers()));
    }
}
