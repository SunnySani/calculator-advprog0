package id.ac.ui.cs.advprog.tutorial4.factory.repository;


import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuRepositoryTest {

    @Mock
    private List<Menu> list;

    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        Menu liyuanSoba = new LiyuanSoba("Liyuan Soba");
        list = new ArrayList<>();
        list.add(liyuanSoba);
    }

    @Test
    public void testWhenGetMenusFunctionCalledItReturnsMenus() {
        ReflectionTestUtils.setField(menuRepository, "list", list);
        List<Menu> menus = menuRepository.getMenus();

        assertThat(menus).isEqualTo(list);
    }

    @Test
    public void testWhenAddFunctionCalledItReturnsMenus() {
        ReflectionTestUtils.setField(menuRepository, "list", list);
        Menu inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");

        menuRepository.add(inuzumaRamen);
        List<Menu> menus = menuRepository.getMenus();

        assertEquals(2, menuRepository.getMenus().size());
        assertThat(menus).isEqualTo(list);
    }
}
