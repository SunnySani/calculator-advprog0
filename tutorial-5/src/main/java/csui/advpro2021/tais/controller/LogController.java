package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLogByMahasiswa(@PathVariable(value = "npm") String npm) {
        return (ResponseEntity<Iterable<Log>>) ResponseEntity.ok(logService.getListLogByMahasiswa(npm));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLogByIdLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/getLogReportMahasiswa/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Map<String, Object>>> getListLogByMonth(@PathVariable(value = "npm") String npm) {
        return (ResponseEntity<Iterable<Map<String, Object>>>) ResponseEntity.ok(logService.getLogReportMahasiswa(npm));
    }
}
