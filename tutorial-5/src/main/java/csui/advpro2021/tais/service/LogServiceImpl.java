package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Iterable<Log> getListLogByMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if(mahasiswa == null) return null;

        List<Log> logs = new ArrayList<>(logRepository.findByMahasiswa(mahasiswa));

        return logs;
    }

    @Override
    public void deleteLogByIdLog(int idLog) {
        Log log = logRepository.findByIdLog(idLog);
        logRepository.delete(log);
    }

    @Override
    public Iterable<Map<String, Object>> getLogReportMahasiswa(String npm) {
        return getReport(getListLogByMahasiswa(npm));
    }

    private Iterable<Map<String, Object>> getReport(Iterable<Log> log) {
        // Initialize result
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        Map<String, Object> temp;

        String[] listMonth = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Semptember",
                              "Oktober","November", "Desember"};
        for (String str: listMonth) {
            temp = new HashMap<>();
            temp.put("Month", str);
            temp.put("jamKerja", new Float(0));
            temp.put("Pembayaran", new Float(0));
            result.add(temp);
        }

        float totalSec;
        float hourSpent;
        float paid;

        String start;
        String end;
        String month;
        int monthInt;
        for (Log item: log) {
            start = item.getStartTime();
            end = item.getEndTime();
            month = start.substring(0,2);
            monthInt = Integer.parseInt(month) - 1;

            temp = result.get(monthInt);
            totalSec = (Float) temp.get("jamKerja") * 3600;

            // Count how long it is from start to end. Count from hour - minute - sec
            totalSec += (Float.parseFloat(end.substring(3,5)) - Float.parseFloat(start.substring(3,5))) * 3600;
            totalSec += (Float.parseFloat(end.substring(6,8)) - Float.parseFloat(start.substring(6,8))) * 60;
            totalSec += (Float.parseFloat(end.substring(9,11)) - Float.parseFloat(start.substring(9,11)));
            hourSpent = totalSec / 3600;
            paid = hourSpent * 350;

            temp.put("jamKerja", hourSpent);
            temp.put("Pembayaran", paid);
        }
        return result;
    }
}
