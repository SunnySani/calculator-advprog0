package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Spy
    OrderService orderService = new OrderServiceImpl();


    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService");
    }

    @Test
    public void testOrderServiceOrderAFoodImplementedCorrectly() throws Exception {
        orderService.orderAFood("Dummy Food");
        assertEquals("Dummy Food", orderService.getFood().getFood());
    }

    @Test
    public void testOrderServiceOrderADrinkImplementedCorrectly() throws Exception {
        orderService.orderADrink("Dummy Drink");
        assertEquals("Dummy Drink", orderService.getDrink().getDrink());
    }
}
