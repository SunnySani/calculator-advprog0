#Perbedaan Lazy Singleton dan Eager Singleton

##1. Inisiasi objek
Dalam <b>Eager Singleton</b>, object class bersifat static dan langsung didkelarasi. Maka dari itu, fungsi getInstance langsung mengembalikan objek tersebut.<br>
Sedangkan dalam <b>Lazy Singleton</b> Objek tidak dideklarasi sebagai object class tersebut. Maka dari itu, fungsi getInstance mengecek terlebih dahulu, apakah object sudah menampung class atau belum. Jika belum, baru object tersebut diinisiasi dengan referensi ke class.

##2. Memori
Lazy Singleton lebih murah memori, karena awal berjalannya program tidak ada object belum memuat class, sehingga memori yang ada dalam class itu tidak banyak (null). Objek yang memuat class tersebut baru akan dibuat saat object itu dibutuhkan.<br>
Sedangkan Eager Singleton, program langsung memiliki object yang memuat class.