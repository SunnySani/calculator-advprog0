package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SweetTest {
    private Class<?> sweetTest;

    @BeforeEach
    public void setUp() throws Exception {
        sweetTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
    }

    @Test
    public void testsweetTestIsPublicClass() {
        assertTrue(Modifier.isPublic(sweetTest.getModifiers()));
    }

    @Test
    public void testsweetTestIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(sweetTest.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testsweetTestOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = sweetTest.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSweetGetDescriptionMethod() throws Exception {
        Sweet sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...",sweet.getDescription());
    }
}
