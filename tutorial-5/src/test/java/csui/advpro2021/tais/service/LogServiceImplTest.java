package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;


    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mahasiswa.setApproved(true);
        log = new Log(1, "01 08:00:00", "01 08:30:00", "test ngajar");
    }

    @Test
    public void testServiceGetListLog(){
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void serviceGetListLogByMahasiswa(){
        mahasiswaService.addLog(mahasiswa.getNpm(), log);
        Iterable<Log> logs = new ArrayList<>(logRepository.findAll());
        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getListLogByMahasiswa(mahasiswa.getNpm())).thenReturn(logs);

        Iterable<Log> logsTest = logService.getListLogByMahasiswa(mahasiswa.getNpm());

        assertEquals(logsTest, logs);

        assertEquals(null, logService.getListLogByMahasiswa("123"));
    }

    @Test
    public void serviceDeleteByIdLog(){
        lenient().when(logRepository.findByIdLog(log.getIdLog())).thenReturn(log);
        logService.deleteLogByIdLog(log.getIdLog());
        List<Log> logTestEmpty = new ArrayList<>();
        assertEquals(logTestEmpty, logService.getListLog());

    }

    @Test
    public void serviceGetLogReportMahasiswa() {

        // add multiple log
        log.setMahasiswa(mahasiswa);
        List<Log> logs = new ArrayList<>(logRepository.findAll());
        logs.add(log);
        logs.add(log);
        //lenient().when(mahasiswaService.addLog(mahasiswa.getNpm(), log)).thenReturn(log);
        mahasiswaService.addLog(mahasiswa.getNpm(), log);

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getListLogByMahasiswa(mahasiswa.getNpm())).thenReturn(logs);
        //lenient().when(mahasiswaService.addLog(mahasiswa.getNpm(), log)).thenReturn(logs);

        logService.getLogReportMahasiswa(mahasiswa.getNpm());
    }

    @Test
    public void servicePrivateGetReport() {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswaService.addLog(mahasiswa.getNpm(), log);

        // Add log
        List<Log> logs = new ArrayList<>();
        logs.add(log);
        logs.add(log);

        lenient().when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.getListLogByMahasiswa(mahasiswa.getNpm())).thenReturn(logs);

        logService.getLogReportMahasiswa(mahasiswa.getNpm());
    }
}
