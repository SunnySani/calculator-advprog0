package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlowerTest {
    private Class<?> flowerClass;

    @BeforeEach
    public void setUp() throws Exception {
        flowerClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
    }

    @Test
    public void testflowerClassIsPublicClass() {
        assertTrue(Modifier.isPublic(flowerClass.getModifiers()));
    }

    @Test
    public void testflowerClassIsATopping() {
        Collection<Type> interfaces = Arrays.asList(flowerClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testflowerClassOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = flowerClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testFlowerGetDescriptionMethod() throws Exception {
        Flower flower = new Flower();
        assertEquals("Adding Xinqin Flower Topping...",flower.getDescription());
    }
}
