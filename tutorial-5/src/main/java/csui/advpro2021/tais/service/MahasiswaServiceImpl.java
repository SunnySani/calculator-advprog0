package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) return;
        if (mahasiswa.isApproved()) return;
        mahasiswaRepository.deleteById(npm);
    }

    @Override
    public Mahasiswa registerCourse(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah matkul = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
        if (mahasiswa != null && matkul != null) {
            mahasiswa.setMataKuliah(matkul);
            mahasiswa.setApproved(true);
            return mahasiswaRepository.save(mahasiswa);
        }
        return null;
    }

    @Override
    public Mahasiswa dropCourse(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) return null;
        mahasiswa.setMataKuliah(null);
        mahasiswa.setApproved(false);
        return mahasiswaRepository.save(mahasiswa);
    }

    @Override
    public Log addLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) return null;
        if (!mahasiswa.isApproved()) return null;
        log.setMahasiswa(mahasiswa);
        return logRepository.save(log);
    }
}
